import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

public class Global 
{
	public static HashSet <String> stopword = new HashSet <String> ();
		
	public static TreeMap <String, Integer> unigram = new TreeMap <String, Integer> (); // To get c(w)
	public static TreeMap <String, Integer> bigram = new TreeMap <String, Integer> (); // To get c(q, w)
	public static HashMap <String, HashMap <String, Long> > cooccurrence = new HashMap <String, HashMap <String, Long> > ();
	
	public static long corpusCount = 347319535860L; // c(corpus)
	
	public static HashMap <String, Long> valueOf_c_w = new HashMap <String, Long> ();
	public static HashSet <String> vertices = new HashSet <String> ();
	
	public static StringBuilder result = new StringBuilder ("");
	

	public static long getCount (String key)
	{
		long count = 0;
		if (valueOf_c_w.containsKey (key))
		 count = valueOf_c_w.get (key);
		return count;			
	}

	public static long getCount (String w1, String w2)
	{
		Long count;
		HashMap <String, Long> cooccur = cooccurrence.get (w1);
		if (cooccur == null)
		{
			cooccur = cooccurrence.get (w2);
			if (cooccur == null)
				count = 0L;
			else
			{
				if (cooccur.containsKey (w1))
					count =  cooccur.get (w1);
				else
					count = 0L;
			}
		}
		else
		{
			if (cooccur.containsKey (w2))
				count =  cooccur.get (w2);
			else
			{
				HashMap <String, Long> cooccur2 = cooccurrence.get (w2);
				if (cooccur2 == null)
				{
					count = 0L;
				}
				else
				{
					if (cooccur2.containsKey (w1))
					{
						count = cooccur2.get (w1);
					}
					else
						count = 0L;
				}		
			}
		}		
		return count;
	}

	public static boolean isStopWord (String str)
	{
		if(stopword.contains(str)) 
			return true;
		return false;
	}
}