import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

import java.util.Comparator;
import java.util.Map;

class Value_Comparator implements Comparator <String> 
{
    Map <String, Long> base;
 
    public Value_Comparator (Map <String, Long> base) 
    {
        this.base = base;
    }
    
    public int compare (String a, String b) 
    {
        if (base.get (a) >= base.get (b)) 
            return -1; 
        else 
            return 1;
    }
}

public class AddingFirstOrderCooccurrence 
{
	Value_Comparator bvc =  new Value_Comparator (GetCoocurrenceWithTread.qCoocurenceMap);
	TreeMap <String, Long> sorted_map = new TreeMap <String,Long> (bvc);
	HashMap <String, Long> unigram = new HashMap <String, Long> (); 
	char pre = ' ';
	
	public HashMap<Character, HashSet<String>>initialize_datastructure(String query) throws IOException 
	{
		long curTime = System.currentTimeMillis();
		long c_q = 0;
		double delta = 0.00009;
		char ch;
		String word = new String();
		c_q = calculate_c_q (query);
		HashMap<Character, HashSet<String>> for_thread= new HashMap<Character, HashSet<String>>();
		ArrayList < Entry <String, Long> > entrySet = new ArrayList < Entry <String, Long> > ();
		entrySet.addAll (GetCoocurrenceWithTread.qCoocurenceMap.entrySet ());
		/*----------------------------adding Bag of words---------------------------------*/
		for (int j = 0; j < Initializer.lemmatized.size(); j++) 
		{
		    String wd = Initializer.lemmatized.get (j);
		    if (GetCoocurrenceWithTread.qCoocurenceMap.containsKey (wd))
		    {
		    	long count = GetCoocurrenceWithTread.qCoocurenceMap.get (wd);
		    	if ((double)(1.0 * count / c_q) >= 0.0000009)
		    	{
		    		ch=wd.charAt(0);
					if(for_thread.containsKey(ch))
					{
						HashSet<String>temp=for_thread.get(ch);
						temp.add(word);
						for_thread.put(ch, temp);
					}
					else
					{
						HashSet<String>temp=new HashSet<String>();
						temp.add(word);
						for_thread.put(ch, temp);
					}
		    	}
		    }
		}
		for ( Entry <String, Long> entry : entrySet)
		{		
			double check = (double) ((1.0) * entry.getValue() / c_q);
			if (check >= delta)
			{
				word = entry.getKey();
				ch=word.charAt(0);
				if(for_thread.containsKey(ch))
				{
					HashSet<String>temp=for_thread.get(ch);
					temp.add(word);
					for_thread.put(ch, temp);
				}
				else
				{
					HashSet<String>temp=new HashSet<String>();
					temp.add(word);
					for_thread.put(ch, temp);
				}
			}
		}
		curTime = System.currentTimeMillis() - curTime;
	//	System.out.println ("Initialize data structure: " + curTime);
		return for_thread;
	}
	
	public void addingCooccurentWordsToVSet (String query,Graph gr) throws IOException 
	{
		long curTime = System.currentTimeMillis();
		long c_q = 0, c_w_q = 0, c_w = 0;
		double delta_dash = 0.000009;
		double dice = 0;
		String word = new String();
		ArrayList < Entry <String, Long> > entrySet = new ArrayList < Entry <String, Long> > ();
		entrySet.addAll (Global.valueOf_c_w.entrySet ());
		for ( Entry <String, Long> entry : entrySet)
		{
				word=entry.getKey();
				c_w_q = entry.getValue();
				c_w=entry.getValue();
				dice = (2.0 * c_w_q) / (c_w + c_q);
				if (dice > delta_dash)
				{
//					System.out.println ("word: " + word + "\tdice: " + dice);
					gr.initialize (word);
				}
		}		
		curTime = System.currentTimeMillis() - curTime;
		//System.out.println ("Adding cooccurant words to vset ends  at: " + curTime);
	}
	public static long calculate_c_q (String input)
	{
		  //System.out.println(" In  calculate ");
		  long curTime = System.currentTimeMillis();
    	  String fname = new String();
		  String line = new String();
	      String word[] = null, frequency = null;
		  long c = 0;
		  fname = "indexes/unigram/" + input.charAt(0);	  
		  
		  try
		  {
			  BufferedReader br = new BufferedReader (new FileReader (fname), 1000 * 1024);
			  word = new String[2];
			  frequency = new String();
			 
			  while ((line = br.readLine()) != null)
			  {
				  word = line.split (" ");
				  frequency = word[1];
				  if (word[0].equals (input))
				  {
					  c += Integer.parseInt (frequency);
					  br.close();
					  return c;
				  }	
			  }
			  word = null;
			  frequency = null;		
			  br.close();
			  curTime = System.currentTimeMillis() - curTime;
	//		  System.out.println ("calculate_c_q ends at: " + curTime);
		  }
		  
		  catch( IOException e )
		  {
				System.out.println(e);
		  }
		  return c;
	}
	
	public long calculate_c_w (String input)
	{
		  //System.out.println(" In  calculate ");
		  long curTime = System.currentTimeMillis();	
    	  String fname = new String();
		  String line = new String();
	      String word[], frequency = null;
	      fname = "indexes/unigram/" + input.charAt(0);		  
		  
	      try
		  {
	    	  BufferedReader br = new BufferedReader (new FileReader (fname), 1000 * 1024);
	    	  word = new String[2];
	    	  frequency = new String();
			
	    	  while ((line = br.readLine()) != null)
	    	  {
	    		  word = line.split (" ");
	    		  frequency = word[1];
	    		  unigram.put (word[0].toString(), Long.parseLong (frequency));
	    		  word = null;
	    	  }
	    	  word = null;
	    	  frequency = null;
	    	  br.close();
	    	  curTime = System.currentTimeMillis() - curTime;
//	    	  System.out.println ("calculate_c_w ends at: " + curTime);
		}
		catch (IOException e)
		{
			System.out.println (e);
		}
		
	    //System.out.println ("input is :" + input);
	      Long temp= 0L;
	      if(unigram.containsKey(input))
	      temp= unigram.get(input).longValue();
	      return temp;
	}
}
