
import java.util.Properties;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

class WordLemmatiser 
{
    protected StanfordCoreNLP pipeline;
    public WordLemmatiser() 
    {
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        this.pipeline = new StanfordCoreNLP(props);
    }
    public String lemmatize(String documentText)
    {	
    	//HashSet<String> lemmas = new HashSet<String>();
    	String lemma = "";
    	Annotation document = new Annotation(documentText);
        
    	this.pipeline.annotate(document);
        
    	java.util.List <CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) 
        {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class))
            {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemma = (String) token.get(LemmaAnnotation.class);        
            }
        }
       // for(int i=0; i<lemmas.size();i++)
      //  {
        	//System.out.println("-------***************************----------------------");
        	//System.out.println("lemmas :"+ lemma);
        	 //System.out.println("-------***************************----------------------");
      //  }
        return lemma;
    }
}
