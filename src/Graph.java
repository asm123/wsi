import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Comparator;

import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

public class Graph 
{
	WeightedGraph <String, DefaultWeightedEdge> graph;
	TreeSet <String> vertex;	// Vertex set of co-occurrence graph
	HashMap <String, Boolean> isVertexVisited; // Required for maximum spanning tree
	HashSet <String> hubs;
	int maxDegree = 0;
	HashMap <String, ArrayList <String> > vertexListMap = new HashMap <String, ArrayList <String> > ();
	HashMap <String, HashSet <String> > visitedPair;
	
	double theta = 0.00005, sigma = 0.001, sigma_dash = 0.0003; // Tuning parameters
	
	public Graph ()
	{
		graph = new SimpleWeightedGraph <String, DefaultWeightedEdge> (DefaultWeightedEdge.class);
		Global.vertices = new HashSet <String> ();
		vertex = new TreeSet <String> ( new Comparator <String> ()
				{
					public int compare (String s1, String s2)
					{
						Long count1 = GetCoocurrenceWithTread.qCoocurenceMap.get(s1);
						Long count2 = GetCoocurrenceWithTread.qCoocurenceMap.get(s2);
						if (count1 >= count2)
							return -1;
						else
							return 1;
					}
				}
			);
		visitedPair = new HashMap <String, HashSet <String> > ();
		isVertexVisited = new HashMap <String, Boolean> (); 
		hubs = new HashSet <String> ();
		maxDegree = 0;
	}
	
	public void initialize (String w)
	{
		if (w.length() < 2 || w.equalsIgnoreCase (Initializer.query))
			return;
		Global.vertices.add (w);
		
		// Add word to vertexListMap required for co-occurrences
	
	//	System.out.println ("vertex added: " + w);
		
		String key = w.substring (0,2).toLowerCase();
		ArrayList <String> tempList = vertexListMap.get (key); 
		if (tempList == null)
			tempList = new ArrayList <String> ();
		tempList.add (w);
		vertexListMap.put (key, tempList);
	}
	
	public void printVertices () 
	{
		System.out.println ("Vertices:");
		for (String v : Global.vertices)
		{
			System.out.println (v);
		}
		System.out.println ("Final vertex set: " + Global.vertices.size());
	}
	
	
	public void createGraph ()
	{
		for (String w1 : Global.vertices)
		{
			for (String w2 : Global.vertices)
			{
				if (w1.equals (w2))
					continue;
				
				if (graph.getEdge (w1, w2) == null)
				{
					
					// Checking if dice had been calculated earlier
					
					HashSet <String> v2Set = visitedPair.get (w1); 
					if (v2Set == null)
					{
						HashSet <String> v1Set = visitedPair.get (w2);
						if (v1Set != null)
						{
							if (v1Set.contains (w1))
								continue;
						}
					}
					else
					{
						if (v2Set.contains (w2))
							continue;
						HashSet <String> v1Set = visitedPair.get (w2);
						if (v1Set != null)
						{
							if (v1Set.contains (w1))
								continue;
						}
					}
										
					double dice = getDice (w1, w2);
	
					if (v2Set == null)
						v2Set = new HashSet <String> ();
					v2Set.add (w2);
					visitedPair.put (w1, v2Set);
					
					if (dice >= theta)
					{
					//	System.out.println("word1:"+w1+" word2:"+w2+" -->dice:"+dice);
						if (!graph.containsVertex (w1))
						{
							graph.addVertex (w1);
							vertex.add (w1);
							isVertexVisited.put (w1, false);
						}
						if (!graph.containsVertex (w2))
						{
							graph.addVertex (w2);
							vertex.add (w2);
							isVertexVisited.put (w2, false);
						}
						
						DefaultWeightedEdge e = graph.addEdge (w1, w2);
						if (e != null)
							graph.setEdgeWeight (e, dice);
					}
				}
			}
		}
	
		
//		System.out.println(" after creating graf vetices are : ");
		for (String v : vertex)
		{
/*			System.out.println(v);
			System.out.println ("Edges with " + v);
			Set <DefaultWeightedEdge> edges = graph.edgesOf (v);
			for (DefaultWeightedEdge edge : edges)
			{
				System.out.println ("Edge " + edge + " Weight: " + graph.getEdgeWeight (edge));
			}
			
			System.out.println ("W: " + v + " Degree: " + degree (v) + " Count: " + Global.getCount (v));
			*/
			maxDegree = Math.max (maxDegree, degree (v));
		}
		
//		Global.vertices.clear();
		visitedPair.clear();
	}
	
	private double getDice (String w1, String w2)
	{
		double dice = 1.0;
		try 
		{
			Long count1 = Global.getCount (w1);
			Long count2 = Global.getCount (w2);
			if (count1 + count2 == 0)
				dice = 1.0;
			else
				dice = (double) 2.0 * Global.getCount (w1, w2) / (Global.getCount (w1) + Global.getCount (w2));
//			System.out.println ("word1: " + w1 + " word2: " + w2 + " dice: " + dice);
		} 
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		return dice;
	}
	
	public void getHubs ()
	{
		HashSet <String> toBeRemoved = new HashSet <String> ();
		
		for (String w : vertex)
		{
		//	System.out.println ("in hub: " + w);
			if (toBeRemoved.contains (w))
			{
			//	System.out.println ("in to be removed: " + w);
				continue;
			}
			
			if (isHub (w))
			{
				hubs.add (w);
				Set <DefaultWeightedEdge> edges = getEdges (w);
				for (DefaultWeightedEdge e : edges)
				{
					String v = graph.getEdgeSource (e);
					if (v.equals (w))
						v = graph.getEdgeTarget (e);
					toBeRemoved.add (v);
				}
			}
			else
				break;
		}
		
		toBeRemoved.clear ();
	}
	
	private boolean isHub (String w)
	{
		double norm_degree = (double)(1.0* degree (w) / maxDegree);
	//	System.out.println ("Vertex: " + w + "Degree: " + (double)degree (w) + " maxdegree: " + maxDegree + " Norm_degree: " + norm_degree);
		if (norm_degree >= sigma)
		{
			double weight_sum = 0;
			Set <DefaultWeightedEdge> edges = getEdges (w);
			for (DefaultWeightedEdge e : edges)
			{
				weight_sum += getWeight (e);
			}
			double avg_weight = 1.0*weight_sum / degree (w);
		//	System.out.println ("Vertex: " + w + " Norm_degree: " + norm_degree + " Avg_weight: " + avg_weight);
			if (avg_weight > sigma_dash)
				return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private int degree (String w)
	{
		return (((AbstractBaseGraph <String, DefaultWeightedEdge>) graph).degreeOf (w));
	}
	
	@SuppressWarnings("unchecked")
	private Set <DefaultWeightedEdge> getEdges (String w)
	{
		return (((AbstractBaseGraph <String, DefaultWeightedEdge>) graph).edgesOf (w));
	}
	
	@SuppressWarnings("unchecked")
	private double getWeight (DefaultWeightedEdge e)
	{
		return (((AbstractBaseGraph <String, DefaultWeightedEdge>) graph).getEdgeWeight (e));
	}
}