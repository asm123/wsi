import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.util.TreeMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Initializer 
{
	static String key = "";
	static ArrayList < HashSet <String> > bag;
	static String query;
	static String stopWordPath = "stopWordsList"; 
	static Graph gr = new Graph ();
	static List <String > lemmatized ;
	TreeMap <String, Integer> bigram = new TreeMap <String, Integer> ();
	TreeMap <String, Long> unigram = new TreeMap <String, Long> ();
	
	AddingFirstOrderCooccurrence vset = new AddingFirstOrderCooccurrence();
	
	static HashMap <Integer, String> labelledClusters = new HashMap <Integer, String> ();
	ArrayList <HashSet <String> > senses;
	
		
	public void start (String q)
	{
		bag = new ArrayList <HashSet <String> > ();
		query =  q;

		createStopwordList ();
		GetCoocurrenceWithTread.getQueryCooccurence (query);
		getSearchResults ();
		getSnippets ();
		
		/* Step 1: Graph construction */
		
		initializeGraph ();
		vset = new AddingFirstOrderCooccurrence ();
		initializeDataStructure();
		addFirstOrderCooccurrence ();
		getW1W2CooccurrenceCount ();
		gr.createGraph ();
			
		/* Step 2: Sense discovery */
		
		getHubs ();
		getSenseClusters ();

		/* Step 3: Labeling the clusters */
			
		labelClusters ();

		bag.clear();
	}
	
	private void getW1W2CooccurrenceCount ()
	{
		GetCooccurrenceWithThread.getWordWordCoocurence (gr.vertexListMap);
		Global.cooccurrence = GetCooccurrenceWithThread.wwCoocurenceMap;
	}
	
	public void createStopwordList ()
	{
		Global.stopword = new HashSet <String> ();
		try
		{
			BufferedReader reader = new BufferedReader (new FileReader (stopWordPath));
			Global.stopword = new HashSet <String> (Arrays.asList (reader.readLine().split (" "))); ;
			reader.close();
		}
		catch (Exception e)
		{
			System.out.println (e +" in getting stop words file");
		}
	}

	private void initializeGraph ()
	{
		StringBuilder textdoc = new StringBuilder();
		int i = 0;
		while (i < bag.size())
		{
			HashSet <String> temp;
			temp = bag.get(i);
			Iterator <String> it = temp.iterator();
			while (it.hasNext())
			{
				String tp = it.next().toString();
				textdoc.append(tp);
				textdoc.append(' ');
			}
			i++;
			it = null;
			temp = null;
		}
		StanfordLemmatizer lem = new StanfordLemmatizer();
		lemmatized = lem.lemmatize (textdoc.toString());
	}
	
	private void initializeDataStructure ()
	{
		HashMap <Character, HashSet <String> > temp 
			= new HashMap <Character, HashSet <String> > ();
		try 
		{
			temp = vset.initialize_datastructure (query);
			GetCooccurrenceWithThread.getWordCount (temp);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void addFirstOrderCooccurrence ()
	{
		/*-----------------------Code for adding first occurance of vertice-------------------------*/
			
		try 
		{
			vset.addingCooccurentWordsToVSet (query, gr);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		/*-------------------------------- end ----------------------------------------------------*/
	}
	
	private void getHubs ()
	{
		gr.getHubs ();
	}
	
	private void getSenseClusters ()
	{
		senses = new ArrayList <HashSet <String> > ();
		FindSenseAndClusters fc = new FindSenseAndClusters (gr, query, bag);
		senses = fc.getSenseClusters();
	}
	
	private void labelClusters ()
	{		
		for (int i = 0; i < senses.size(); i++)
		{
			HashSet <String> cluster = senses.get (i);
			String label = new String ();
			try 
			{
				ClusterLabeler cl = new ClusterLabeler ();
				label = cl.getLabelForCluster (cluster);
				labelledClusters.put (i, label);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}

	private void getSearchResults ()
	{
		BufferedReader br;
		URL url;
		try 
		{
			url = new URL ("https://www.googleapis.com/customsearch/v1?key=" + key + "&cx=013036536707430787589:_pqjad5hr1a&q=" + query + "&alt=json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setRequestMethod ("GET");
	        conn.setRequestProperty ("Accept", "application/json");
	        br = new BufferedReader (new InputStreamReader ((conn.getInputStream ())));     
	        File f = new File ("results");
	        if (f.exists ())
	        	f.delete ();        
	        BufferedWriter bw = new BufferedWriter (new FileWriter (f, true));
	        String line;
	        while ((line = br.readLine ()) != null) 
	        {
	        	bw.write (line);
	            bw.newLine ();
	        }        
	        bw.close ();
		} 
		catch (Exception e)
		{
			System.out.println ("Waiting for connection...");
		}        
	}
	
	private void getSnippets ()
	{
		JSONParser parser = new JSONParser();	
		try 
		{
			Object obj = parser.parse (new FileReader ("results"));
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray items = (JSONArray) jsonObject.get ("items");
			Iterator <?> iterator = items.iterator ();
			while (iterator.hasNext ()) 
			{
				JSONObject jsonObject1 = (JSONObject) iterator.next ();
				String snippet = (String) jsonObject1.get ("snippet");
				createBag (snippet);
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void createBag (String snippet) throws IOException 
	{
		snippet = snippet.toLowerCase ();	
		HashSet <String> words = new HashSet <String> ();
		String word = new String ();
		for (int i = 0; i < snippet.length(); i++)
		{
			char c = snippet.charAt (i);
			if ((c >= 'a' && c <= 'z'))
				word += c;
			else
			{
				if (word.length() > 0)
				{
					if (!Global.stopword.contains (word) && !word.equals (query))
					{
						words.add (word.toLowerCase());
					}
				}
				word = new String ();
			}
		}
		
		// Add to corresponding bag
		
		bag.add (words);
	}	
}
