import java.util.LinkedList;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

class StanfordLemmatizer 
{
    protected StanfordCoreNLP pipeline;
    public StanfordLemmatizer() 
    {
        // Create StanfordCoreNLP object properties, with POS tagging
        // (required for lemmatization), and lemmatization
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        // StanfordCoreNLP loads a lot of models, so you probably
        // only want to do this once per execution
        this.pipeline = new StanfordCoreNLP(props);
    }
    public java.util.List<String> lemmatize(String documentText)
    {
    	java.util.List<String> lemmas = new LinkedList<String>();
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        // run all Annotators on this text
        this.pipeline.annotate(document);
        // Iterate over all of the sentences found
        java.util.List <CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) 
        {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class))
            {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.add((String) token.get(LemmaAnnotation.class));        
            }
        }
       /* for(int i=0; i<lemmas.size();i++)
        {
        	//System.out.println("-------***************************----------------------");
        //	System.out.println(lemmas.get(i));
        	 //System.out.println("-------***************************----------------------");
        }*/
        return lemmas;
    }
}

