import java.util.*;

import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

class ValueComparator implements Comparator <String> 
{
	Map <String, Integer> base;
    
    public ValueComparator (HashMap <String, Integer> mstGraph) 
    {
        this.base = mstGraph;
    }
    
    public int compare (String a, String b) 
    {
        if (base.get(a) >= base.get(b)) 
        {
        	return 1;
        } 
        else 
        {
            return -1;
        } // returning 0 would merge keys
    }
}

class SimilarityComparator implements Comparator <Integer> 
{

    Map <Integer, Pair> base;
    
    public SimilarityComparator (Map <Integer, Pair> base) 
    {
        this.base = base;
    }

    
    public int compare (Integer a, Integer b) 
    {
    	float avgSim1 = base.get(a).totalSimilarity / base.get(a).countTotal;
    	float avgSim2 = base.get(b).totalSimilarity / base.get(b).countTotal;
    	
        if (avgSim1 >= avgSim2) 
        {
            return 1;
        } 
        else 
        {
            return -1;
        } // returning 0 would merge keys
    }
}

class Pair
{
	float totalSimilarity;
	int countTotal;
	HashSet<String> words = new HashSet<String>();
	Pair (float totalSimilarity , int countTotal, HashSet<String> words) 
	{
		this.totalSimilarity = totalSimilarity;
		this.countTotal = countTotal;
		this.words = words;
	}
}

class FindSenseAndClusters
{
	static ArrayList <HashSet <String> > senses;
	static HashSet <String> hubs;
	
	public static void sortClusters (HashMap <Integer, Pair> clusterSimilarites)
	{
		SimilarityComparator sc = new SimilarityComparator (clusterSimilarites);
		TreeMap <Integer, Pair> sortedClusters = new TreeMap <Integer, Pair> (sc);
		sortedClusters.putAll (clusterSimilarites);
				
		/*System.out.println ("Senses:");
		for (int i = 0; i < senses.size(); i++)
		{
			System.out.println ((i + 1) + ": " + senses.get (i));
		}
		
		System.out.println ("Sorted Clusters: ");
		for ( Integer i : sortedClusters.keySet() )
		{
			System.out.println("Cluster "+ i + " : " + hubs.toArray()[i-1] + " has results :" + clusterSimilarites.get(i).words);
		}*/
		
		Global.result.append ("Senses:\n");
		for (int i = 0; i < senses.size(); i++)
		{
			Global.result.append ((i + 1) + ": " + senses.get (i) + "\n");
		}
		
		Global.result.append ("\nSorted Clusters:\n");
		for ( Integer i : sortedClusters.keySet() )
		{
			Global.result.append ("Cluster "+ i + " : " + hubs.toArray()[i-1] + " has results :" + clusterSimilarites.get(i).words + "\n");
		}
	}
	
	public static void clusterWebSearchResults (ArrayList <HashSet <String> > bag)
	{
		float maxSim = 0;

		HashMap <Integer, Pair> clusterSimilarites = new HashMap <Integer, Pair> ();

		int cluster = 0;
		for(HashSet <String> b : bag)
		{
			HashSet <String> tempSimSet = new HashSet <String> ();
			HashSet <String> res = new HashSet <String> ();
			tempSimSet.addAll(b);
			maxSim = 0;
			float tempSim = 0;
			
			cluster = 0;
			for (int j = 0 ; j < senses.size();j++)
			{
				HashSet <String> sense = senses.get(j);
				tempSimSet.retainAll (sense);
				res.addAll(tempSimSet);

				tempSim = ((float) tempSimSet.size()) / b.size(); 
				if (maxSim == 0 && tempSim != 0)
				{
					maxSim = tempSim;
					cluster = j + 1;
				}
				else
				{
					if (tempSim > maxSim)
						maxSim = tempSim;
				}
				tempSimSet.clear();
				tempSimSet.addAll(b);
			}
			
			//clusters.add(cluster);
			if(clusterSimilarites.containsKey(cluster)){
				Pair pair = clusterSimilarites.get(cluster);
				HashSet<String> words = new HashSet<String>(pair.words);
				words.addAll(res);
				pair.totalSimilarity += maxSim;
				pair.countTotal++;
				pair.words = words;
//				System.out.println("*2* : results : " + pair.words);
				clusterSimilarites.put(cluster, pair);
			}
			else if(cluster > 0)
			{
//				System.out.println("*1* : results : " + res);
				clusterSimilarites.put(cluster, new Pair(maxSim,1, new HashSet<String>(res)));
			}
//			System.out.println("max sim :"+maxSim);
		}
	//	System.out.println("clusters :"+ clusters);
//		System.out.println("clusters and similarities :"+ clusterSimilarites);
		sortClusters(clusterSimilarites);
	}
	
	public static void calculateMST (Graph g, String q)
	{
		HashSet <String> vertexSet = new HashSet <String> (g.graph.vertexSet());
		//TreeSet<DefaultWeightedEdge> edgeSet = new TreeSet<DefaultWeightedEdge>();
		
		final WeightedGraph <String, DefaultWeightedEdge> graph = g.graph;
		
		Queue <DefaultWeightedEdge> sortedEdges = new PriorityQueue <DefaultWeightedEdge> (vertexSet.size(), new Comparator <DefaultWeightedEdge> () 
				{
		    @Override
		    public int compare (DefaultWeightedEdge o1, DefaultWeightedEdge o2) 
		    {
		      if (graph.getEdgeWeight(o1) < graph.getEdgeWeight(o2)) 
		      {
		        return 1;
		      }
		      if (graph.getEdgeWeight(o1) > graph.getEdgeWeight(o2)) 
		      {
		        return -1;
		      } 
		      return 0;
		    }
		  });
		//System.out.println("in calculate mst..");
		sortedEdges.addAll (graph.edgeSet());
		//System.out.println("sorted edges :"+ sortedEdges.size());
		
		int nVertex = vertexSet.size();
		
		HashSet <DefaultWeightedEdge> mstEdgeSet = new HashSet <DefaultWeightedEdge> ();
	//	int mstEdgeCount = 0;
		Iterator <String> it = vertexSet.iterator();
		for(int i = 0; i < nVertex; i++)
		{
			g.isVertexVisited.put ((it.next()).toLowerCase(), false);
		}
		
		Integer clusterCount = 0; 
		HashMap <String, Integer> mstGraph = new HashMap <String, Integer> (); 
		ValueComparator bvc =  new ValueComparator (mstGraph);
		TreeMap <String, Integer>  mstSenses = new TreeMap <String, Integer> (bvc);
		int mstEdgeCount = 0;
		boolean flag;
		while (!sortedEdges.isEmpty())
		{
			DefaultWeightedEdge edge = sortedEdges.poll();
			String source = (graph.getEdgeSource (edge)).toLowerCase();
			String target = (graph.getEdgeTarget (edge)).toLowerCase();
			flag = false;
			if (!((g.isVertexVisited.get (source) & g.isVertexVisited.get(target))) )
			{
				flag = true;
			}
			else if ( mstGraph.containsKey(target) && mstGraph.containsKey(source) )
			{
				int cluster1 = mstGraph.get(source);
				int cluster2 = mstGraph.get(target);

				if((Math.max(cluster1, cluster2) == clusterCount)&&(clusterCount > g.hubs.size()))
				{
					mstEdgeCount++;
					if(cluster1 == clusterCount)
					{
						mstGraph.put(source, cluster2);
						for(String key : mstGraph.keySet())
						{
							if(mstGraph.get(key) == clusterCount)
							{
								mstGraph.put(key, cluster2);
							}
						}
					}
					else
					{
						mstGraph.put(target, cluster1);
						for(String key : mstGraph.keySet())
						{
							if(mstGraph.get(key) == clusterCount)
							{
								mstGraph.put(key, cluster1);
							}
						}
					}
					clusterCount--;
				}
					flag = false;
			}
			if(flag)
			{
				if((source.equals(q))|| !(mstGraph.containsKey(source)) && !(mstGraph.containsKey(target)) )
				{
					mstEdgeSet.add(edge);
					g.isVertexVisited.put(source, true);
					g.isVertexVisited.put(target, true);
					
					mstEdgeCount++;
					
					clusterCount++;
					if(!source.equals(q))
						mstGraph.put(source, clusterCount);
					mstGraph.put(target, clusterCount);
				}
				else if(mstGraph.containsKey(source))
				{
					mstEdgeSet.add(edge);
					
					g.isVertexVisited.put(target, true);
					mstEdgeCount++;
					int tempCount = mstGraph.get(source);
					mstGraph.put(target, tempCount);
				}
				else
				{
					mstEdgeSet.add(edge);
					
					g.isVertexVisited.put(source, true);
					mstEdgeCount++;
					
					int tempCount = mstGraph.get(target);
					mstGraph.put(source, tempCount);
				}
			}
			if((mstEdgeCount+ g.hubs.size() >= nVertex) && (mstGraph.size() == nVertex - 1))
				break;
		}

		mstSenses.putAll(mstGraph);
		mstGraph.clear();
		
		senses = new ArrayList<HashSet<String>>(); 		
		Iterator<Map.Entry<String, Integer>> iter = mstSenses.entrySet().iterator();
		HashSet<String> tempSet = new HashSet<String>();
		
		for(Integer i = 1 ; i <= g.hubs.size(); i++)
		{
			String key;
			for( ; iter.hasNext(); )
			{
				Map.Entry<String, Integer> entry = iter.next();

				if(entry.getValue() == i)
				{
					key = entry.getKey();
					tempSet.add(key);
				}
				else
				{
					senses.add(tempSet);
					tempSet = new HashSet<String>();
					tempSet.add(entry.getKey());
					break;
				}
			}
		}
		senses.add(tempSet);
		mstSenses.clear();
	}
	
	public ArrayList < HashSet <String> > getSenseClusters ()
	{
		return senses;
	}
	
	public FindSenseAndClusters (Graph g, String query, ArrayList < HashSet <String> > bag)
	{		
		g.graph.addVertex (query);
		hubs = g.hubs;
		for (String hub : g.hubs)
		{
			DefaultWeightedEdge newEdge = g.graph.addEdge (query, hub);
			if (newEdge != null)
				g.graph.setEdgeWeight (newEdge, 2.0);
		}
		
		calculateMST (g, query);
		
		/*StanfordLemmatizer sl = new StanfordLemmatizer();
		ArrayList< HashSet <String>> lemmatisedBag = new ArrayList< HashSet <String>>(); 
		for (HashSet<String> b : bag){
			lemmatisedBag.add(sl.lemmatize(b.toString()));
		}
		*/
		
		clusterWebSearchResults(bag);
	}
}