import java.util.HashSet;

public class ClusterLabeler 
{
	HashSet <String> candidateLabels;
	HashSet <String> importantTerms;
	String label;
	
	int totalCountOfCluster = 0;
	
	ClusterLabeler ()
	{
		importantTerms = new HashSet <String> ();
		candidateLabels = new HashSet <String> ();
		label = new String ();
	}
	
	public String getLabelForCluster (HashSet <String> cluster)
	{
		initialize (cluster);
		findLabel ();
		return label;
	}
	
	private void initialize (HashSet <String> cluster)
	{
		importantTerms.addAll (cluster);
		candidateLabels.addAll (cluster);
		
		for (String term : importantTerms)
		{
			totalCountOfCluster += Global.getCount (term);
		}
	}
	
	private void findLabel ()
	{
		double maxMI = 0.0;
		
		for (String l : candidateLabels)
		{
			double mi = 0;
			for (String t : importantTerms)
			{
				if (l.equals (t))
					continue;
				mi += PMI (l, t) * weight (t);
			}
			
			if (mi >= maxMI)
			{
				maxMI = mi;
				label = l; 
			}
		}
	}
	
	// Pointwise mutual information 
	
	private double PMI (String l, String t)
	{
		double pmi;
		double pr_l = Pr (l);
		double pr_t = Pr (t);
		
		if (pr_l == 0 || pr_t == 0)
			pmi = 0;
		else
		{
			double pr_l_t = Pr (l, t);
			pmi = Math.log (pr_l_t / (pr_l * pr_t));
		}
		
		return pmi;
	}
	
	// Probability of term w in corpus
	
	private double Pr (String w)
	{
		double pr = (double) Global.getCount (w) / Global.corpusCount;
		return pr;
	}
	
	// Probability of co-occurrence of terms w1 & w2 in corpus
	
	private double Pr (String w1, String w2)
	{
		double pr = (double) Global.getCount (w1, w2) / Global.corpusCount;
		return pr;
	}
	
	private double weight (String term)
	{
		double wt = (double) Global.getCount (term) / totalCountOfCluster;
		return wt;
	}
}