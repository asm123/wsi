
import java.util.*;
import java.io.*;

class NewThread extends Thread
{
	String fileName, match;
    
	public NewThread (String fileName, String match)
    {
    	super (fileName);
    	this.fileName = fileName;
    	this.match = match;
    }
	
	public void run()
    {
    	 try
    	 {
    		 BufferedReader br = new BufferedReader (new FileReader(fileName));
    		 String line = "", w1 = "", w2 = "";
    		 String lineTok [];
    		 long count;
    		 while (((line = br.readLine()) != null) /*&& (tempw1.compareToIgnoreCase(w1) < 0)*/)
    		 {
    			 lineTok = line.split ("[ ]");
    			 w1 = lineTok[0].toLowerCase();
    			 w2 = lineTok[1].toLowerCase();
    			 count = Long.parseLong (lineTok[2]);
    			 
    			 if ((w1.compareToIgnoreCase (match) == 0) && (!Global.isStopWord (w2)) )
    			 {
    				 GetCoocurrenceWithTread.writeToMap (w1, w2, count);
    			 }
  				
    			 if ((w2.compareToIgnoreCase (match) == 0) && (!Global.isStopWord (w1)))
    			 {
    				 GetCoocurrenceWithTread.writeToMap (w2, w1, count);
  				 }
  		    }
  		    br.close();
    	 }
    	 
    	 catch(Exception e)
    	 {
  		   	e.printStackTrace();
    	 }
    }
}

class GetCoocurrenceWithTread 
{
	static TreeMap <String, Long> qCoocurenceMap = new TreeMap <String, Long> ();
	static List <String> sw;
	
	static synchronized void writeToMap (String w1, String w2, long count)
	{
		if (qCoocurenceMap.containsKey (w2))
		{
			count = qCoocurenceMap.get(w2) + count;
			qCoocurenceMap.put(w2, count);
		}
		else 
		{
			qCoocurenceMap.put(w2, count);
		}
		//System.out.println(w1 + " found with "+ w2 + " "+ count + " times");
	}
	
	static void getQueryCooccurence (String query)
	{
		//getStopwords();
		ArrayList <NewThread> threadPool = new ArrayList <NewThread> (); 
		try
		{
			String dirPath;
			long curTime = System.currentTimeMillis();
			
			for (char fname = 'a'; fname <= 'z'; fname++)
			{
				dirPath = "indexes/bigram/" + fname;
				//System.out.println(dirPath);
				
				String files [] = (new File(dirPath)).list();
				//Arrays.sort(files);
				
				for(String f : files)
				{
					String fileName = dirPath + "/" + f;
					
					NewThread thread = new NewThread (fileName, query);
					threadPool.add (thread);
			     	thread.start();
				}   
			}
		//	System.out.println("threads in thread pool : "+threadPool.size());
			
			for (NewThread t = threadPool.get(0); threadPool.size() != 0;)
			{
	        	if (!t.isAlive())
	        		threadPool.remove(t);
	        	if (threadPool.size() != 0)
	        		t = threadPool.get(0);
	        }
	        
	      //  System.out.println("map : \n" + qCoocurenceMap);
	        
			curTime = System.currentTimeMillis() - curTime;
	//		System.out.println ("time elapsed in sec:" + (double) curTime / 1000);
			
			/*long size = 0L;
			for(String key : qCoocurenceMap.keySet()){
//				System.out.println(key + " : " + qCoocurenceMap.get(key));
				size += qCoocurenceMap.get(key);
			}*/
		//	System.out.println("sizeof corpus : "+ size + " no of unique terms : " +  qCoocurenceMap.size());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*public static void main(String args[]){
		//String query = "apple";
		//getQueryCooccurence(query);
		
	}*/
}
