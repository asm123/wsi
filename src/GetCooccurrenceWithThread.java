import java.util.*;
import java.io.*;

class QueryCooccurrenceThread extends Thread
{
	String fileName, match;

	public QueryCooccurrenceThread (String fileName, String match)
	{
		super (fileName);
		this.fileName = fileName;
		this.match = match;
	}

	public void run ()
	{
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = "", w1 = "", w2 = "";
			String lineTok[];
			long count;
			while (((line = br.readLine()) != null)) 
			{
				lineTok = line.split("[ ]");
				w1 = lineTok[0].toLowerCase();
				w2 = lineTok[1].toLowerCase();
				count = Long.parseLong(lineTok[2]);

				if (w1.compareToIgnoreCase(match) == 0) {
					GetCooccurrenceWithThread.writeToMap(w1, w2, count);
				}
				if (w2.compareToIgnoreCase(match) == 0) {
					GetCooccurrenceWithThread.writeToMap(w2, w1, count);
				}
			}
			br.close();
		} catch (Exception e) 
		{
	//		e.printStackTrace();
		}
	}
}

class UnigramThread extends Thread
{
	char firstChar;
	String dir = "indexes/unigram";
	
	UnigramThread (char c)
	{
		super ();
		firstChar = c;
	}
	
	public void run ()
	{
		try 
		{
//			System.out.println ("Thread " + firstChar + " starts here.");
			HashSet <String> set = GetCooccurrenceWithThread.charWordMap.get (firstChar);
			if (set == null)
				return;
			BufferedReader br = new BufferedReader (new FileReader (dir + "/" + firstChar));
			String line = new String ();
			while ((line = br.readLine()) != null)
			{
				String [] temp = line.split ("\\s+");
				String word = temp[0];		
				if (set.contains (word))
				{
					String frequency = temp[1];
					long count = Long.parseLong (frequency);
					Global.valueOf_c_w.put (word, count);
				}
			}
			br.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}

class BigramThread extends Thread
{
	String fileName;
	ArrayList<String> w1List = new ArrayList<String>();
	ArrayList<ArrayList<String>> w2List = new ArrayList<ArrayList<String>>();
	public BigramThread(String fileName, ArrayList<String> w1List, ArrayList<ArrayList<String>> w2List)
	{
		super(fileName);
		this.fileName = fileName;
		this.w1List = w1List;
		this.w2List = w2List;
	}

	public void run()
     {
    	 try{
  		   BufferedReader br = new BufferedReader (new FileReader(fileName));
  		   String line = "", w1 = "", w2 = "";
  		   String lineTok[];
  		   long count;
  		 HashSet<String> tempw1List =  new HashSet<String>();;
  		 HashSet<String> tempw2List = new HashSet<String>();;
  		 ArrayList<String> w2listOfLists = new ArrayList<String>(); 
  		 for ( ArrayList<String> list : w2List )
  		 {
  			w2listOfLists.addAll(list);
  		 }
  		 tempw1List.addAll(w1List);
		 tempw2List.addAll(w2listOfLists);		 
			// System.out.println(fileName + "is reading a file ");
  		 while (((line = br.readLine()) != null))
  		 {
  			// tempw1List.clear();
  			// tempw2List.clear();
  			 lineTok = line.split("[ ]");
  			 w1 = lineTok[0].toLowerCase();
  			 w2 = lineTok[1].toLowerCase();
  			 count = Long.parseLong(lineTok[2]);
  			 
  				if ( (tempw1List.contains(w1)) && (tempw2List.contains(w2)) || ( (tempw1List.contains(w2)) && (tempw2List.contains(w1))) ){
//  					System.out.println("*1* w1 :" + w1 + " w2 : " + w2);
  					GetCooccurrenceWithThread.writeToWWMap( w1, w2, count );
  				}
  				else if ( tempw1List.contains(w1) ){
  				//	tempw1List.remove(w1);
  					//System.out.println("*12* w1 :" + w1 + " w2 : " + w2);
  					//System.out.println("w1List : " + tempw1List + " \n w2list " + tempw2List);
  					//System.out.println();
  					if ( tempw1List.contains(w2) &&(!w1.equals(w2)) ){
//  						System.out.println("*2* w1 :" + w1 + " w2 : " + w2);
  	  					GetCooccurrenceWithThread.writeToWWMap( w1, w2, count );
  					}
  					//tempw1List.clear();
  	  		    	
  	  		    	//tempw1List.addAll(w1List);
  	  		    	
  					//System.out.println("*12* w1List : " + tempw1List + " \n w2list " + tempw2List);
  					//System.out.println();
  				}
  				else if (tempw1List.contains(w2)){
  					//tempw1List.remove(w2);
  					//System.out.println("*13* w1 :" + w1 + " w2 : " + w2);
  					if ( tempw1List.contains(w1)&&(!w1.equals(w2)) )
  					{
//  						System.out.println("*3* w1 :" + w1 + " w2 : " + w2);
  	  					GetCooccurrenceWithThread.writeToWWMap( w1, w2, count );
  					}
  					//tempw1List.clear();
  	  		    	
  	  		    	//tempw1List.addAll(w1List);
  	  		    	
  	  		  		
  				}
  				else if ( tempw2List.contains(w1) ){
  					//tempw2List.remove(w1);
  					//System.out.println("*14* w1 :" + w1 + " w2 : " + w2);
  					if ( tempw2List.contains(w2)&&(!w1.equals(w2)) ){
//  						System.out.println("*4* w1 :" + w1 + " w2 : " + w2);
  	  					GetCooccurrenceWithThread.writeToWWMap( w1, w2, count );
  					}
  					
  	  		    	//tempw2List.clear();
  	  		    	
  	  		    	//tempw2List.addAll(w2listOfLists);
  				}
  				else if (tempw2List.contains(w2)){
  					//tempw2List.remove(w2);
  					//System.out.println("*15* w1 :" + w1 + " w2 : " + w2);
  					if ( tempw2List.contains(w1) &&(!w1.equals(w2))){
//  						System.out.println("*5* w1 :" + w1 + " w2 : " + w2);
  	  					GetCooccurrenceWithThread.writeToMap( w1, w2, count );
  					}
  					
  					
  	  		    	//tempw2List.clear();
  	  		    	
  	  		    	//tempw2List.addAll(w2listOfLists);
  				}
  				
  				/*if( (w1List.contains(w1) && (w1List.contains(w2)) ) || ((w2List.contains(w1)) &&(w2List.contains(w2)))
  			    || ( (w1List.contains(w1)) && (w2List.contains(w2))) || ( (w1List.contains(w2)) && (w2List.contains(w1))) ){
  					System.out.println("w1 :" + w1 + " w2 : " + w2);
  					GetCoocurrenceWithThread.writeToMap( w1, w2, count );
  				}*/
  				/*if( w1List.contains(w2)){
  					//System.out.println("in file *1*: " + fileName);
  					if(w2List.contains(w1))
  						GetCoocurrenceWithThread.writeToMap( w1, w2, count );
  				}
  				if( w2List.contains(w1) ){
  					//System.out.println("in file *1*: " + fileName);
  					if(w1List.contains(w2))
  						GetCoocurrenceWithThread.writeToMap( w1, w2, count );
  				}
  				if( w2List.contains(w2)){
  					//System.out.println("in file *1*: " + fileName);
  					if(w1List.contains(w1))
  						GetCoocurrenceWithThread.writeToMap( w1, w2, count );
  				}*/
  		    }
  		    br.close();
  		//  System.out.println(fileName + "done  reading a file ");
    	 }
  		  catch(Exception e){
  		//   	e.printStackTrace();
  		   }
    }
}

/*
 * class FrequencyComparator implements Comparator<String> {
 * 
 * Map<String, Long> base; public FrequencyComparator(Map<String, Long> base) {
 * this.base = base; }
 * 
 * // Note: this comparator imposes orderings that are inconsistent with equals.
 * public int compare(String a, String b) { if (base.get(a) >= base.get(b)) {
 * return -1; } else { return 1; } // returning 0 would merge keys } }
 */
class GetCooccurrenceWithThread 
{

	static HashMap<String, Long> qCoocurenceMap = new HashMap<String, Long>();
	static HashMap<String, HashMap<String, Long>> wwCoocurenceMap = new HashMap<String, HashMap<String, Long>>();
	
	static HashMap <Character, HashSet <String> > charWordMap;
	
	
	// static TreeMap<String, Long> qSortedCoocurenceMap = new TreeMap<String,
	// Long>();

	static synchronized void writeToMap(String w1, String w2, long count) {
		if (qCoocurenceMap.containsKey(w2)) {
			count = qCoocurenceMap.get(w2) + count;
			qCoocurenceMap.put(w2, count);
		} else {
			qCoocurenceMap.put(w2, count);
		}
		// System.out.println(w1 + " found with "+ w2 + " "+ count + " times");
	}

	static synchronized void writeToWWMap(String w1, String w2, long count) {
		if (wwCoocurenceMap.containsKey(w2)) {
			HashMap<String, Long> tempMap = wwCoocurenceMap.get(w2);
			if (tempMap.containsKey(w1)) {
				count = tempMap.get(w1) + count;
				tempMap.put(w1, count);
				wwCoocurenceMap.put(w2, tempMap);
			} else {
				// HashMap<String, Long> newMap = new HashMap<String, Long>();
				tempMap.put(w1, count);
				wwCoocurenceMap.put(w2, tempMap);
			}
		} else if (wwCoocurenceMap.containsKey(w1)) {
			HashMap<String, Long> tempMap = wwCoocurenceMap.get(w1);
			if (tempMap.containsKey(w2)) {
				count = tempMap.get(w2) + count;
				tempMap.put(w2, count);
				wwCoocurenceMap.put(w1, tempMap);
			} else {
				// HashMap<String, Long> newMap = new HashMap<String, Long>();
				tempMap.put(w2, count);
				wwCoocurenceMap.put(w1, tempMap);
			}
		} else {
			HashMap<String, Long> newMap = new HashMap<String, Long>();
			newMap.put(w2, count);
			wwCoocurenceMap.put(w1, newMap);
		}
		// System.out.println(w1 + " found with "+ w2 + " "+ count + " times");
	}

	static void getQueryCooccurence(String query) 
	{
		ArrayList<QueryCooccurrenceThread> threadPool = new ArrayList<QueryCooccurrenceThread>();
		// TreeMap<String,Long> sorted_map = null;
		try {
			String dirPath;
			long curTime = System.currentTimeMillis();

			for (char fname = 'a'; fname <= 'z'; fname++) 
			{
				dirPath = "indexes/bigram/" + fname;
				// System.out.println(dirPath);

				String files[] = (new File(dirPath)).list();
				// Arrays.sort(files);

				for (String f : files) 
				{
					String fileName = dirPath + "/" + f;

					QueryCooccurrenceThread thread = new QueryCooccurrenceThread(fileName, query);
					threadPool.add(thread);
					thread.start();
				}
			}
		//	System.out.println("threads in thread pool for bigram co-occurrence: " + threadPool.size());

			for (QueryCooccurrenceThread t = threadPool.get(0); threadPool.size() != 0;) {
				if (!t.isAlive())
					threadPool.remove(t);
				if (threadPool.size() != 0)
					t = threadPool.get(0);
			}

			// System.out.println("map : \n" + qCoocurenceMap);

			curTime = System.currentTimeMillis() - curTime;
		//	System.out.println("time elapsed in sec: " + (double) curTime
			//		/ 1000);

			
			
		} catch (Exception e) {
		//	e.printStackTrace();
		}
		// return sorted_map;
	}

	static void getWordWordCoocurence(HashMap<String, ArrayList<String>> vertexListMap)
	{
		ArrayList<BigramThread> threadPool = new ArrayList<BigramThread>();
		ArrayList<ArrayList<String>> listLists = new ArrayList<ArrayList<String>>();
		// listLists = vertexListMap.values();
		try {
			String fileName;// , dirPath2;
			long curTime = System.currentTimeMillis();
			ArrayList<String> w1List = new ArrayList<String>();
			ArrayList<ArrayList<String>> w2List = new ArrayList<ArrayList<String>>();
			for (String fname : vertexListMap.keySet()) 
			{
				// String w1 = vertexSet.get(i);
				fileName = "indexes/bigram/" + fname.charAt(0) + "/" + fname;
				w1List = vertexListMap.get(fname);
				listLists = new ArrayList<ArrayList<String>>(
						vertexListMap.values());
				listLists.remove(w1List);
				w2List = new ArrayList<ArrayList<String>>(listLists);

				BigramThread thread = new BigramThread(fileName, w1List, w2List);
				//System.out.println(fileName+ "thread created");
				threadPool.add(thread);
				thread.start();

				listLists.clear();
			}
		//	System.out.println ("threads in thread pool : " + threadPool.size());

			for (BigramThread t = threadPool.get(0); threadPool.size() != 0;) {
				if (!t.isAlive())
					threadPool.remove(t);
				if (threadPool.size() != 0)
					t = threadPool.get(0);
			}
			
			curTime = System.currentTimeMillis() - curTime;
		//	System.out.println ("time elapsed in sec: " + (double) curTime / 1000);
		} 
		catch (Exception e) 
		{
		//	e.printStackTrace();
		}
	}
	
	public static void getWordCount (HashMap <Character, HashSet <String> > map)
	{		
		try
		{			
			charWordMap = map;
			ArrayList <UnigramThread> threads = new ArrayList <UnigramThread> ();
			for (char c = 'a'; c <= 'z'; c++)
			{
				if (charWordMap.containsKey (c))
				{
					UnigramThread t = new UnigramThread (c);
					threads.add (t);
					t.start();
				//	System.out.println ("Thread for " + c + " created.");
				}
			}
			
			while (threads.size() != 0)
			{
				UnigramThread t = threads.get (0);
				if (!t.isAlive())
					threads.remove (t);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace ();
		}
	}
}
