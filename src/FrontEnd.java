import java.awt.event.*;
import javax.swing.*;


class FrontEnd extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	JLabel lQuery;
	JTextField tQuery;
	JButton submit, reset;
	JTextArea results;
	JScrollPane scroll;
	
	String query = new String ();

	public FrontEnd ()
	{		
		setSize (670, 900);
		setVisible (true);
		setLayout (null);		
		
		lQuery = new JLabel ("Query"); add (lQuery);
		tQuery = new JTextField (); add (tQuery);
		
		submit = new JButton ("Submit"); add (submit);
		reset = new JButton ("Reset"); add (reset);
		
		lQuery.setBounds (50, 50, 50, 20); add (lQuery);
		tQuery.setBounds (120, 50, 100, 20); add (tQuery);
		
		submit.setBounds (50, 150, 90, 25); add (submit);
		reset.setBounds (170, 150, 90, 25); add (reset);
		
		results = new JTextArea ();
		results.setEditable (false); add (results);
		scroll = new JScrollPane (results); 
		scroll.setBounds (50, 280, 600, 600); add (scroll);
		
		submit.addActionListener ((ActionListener) this);
		reset.addActionListener ((ActionListener) this);	
		
		addWindowListener (new WindowAdapter ()
		{
			public void windowClosing (WindowEvent we)
			{
				results.setText ("");
				System.exit (0);
			}
		});
	}
	
	public void actionPerformed (ActionEvent ae)
	{
		if (ae.getSource() == submit)
		{			
			query = tQuery.getText().trim();
			if (query.length() > 0)
			{
				Initializer init = new Initializer ();
				System.out.println ("Inducing senses...");
				results.setEnabled (true);
				init.start (query);
				results.setText (Global.result.toString());
			}
		}
		else if (ae.getSource() == reset)
		{
			tQuery.setText ("");
			results.setText ("");
		}
	}
}